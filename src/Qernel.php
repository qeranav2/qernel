<?php
declare(strict_types=1);

namespace Qerana\Core;

use Exception;
use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Controller\ArgumentResolverInterface;
use Symfony\Component\HttpKernel\Controller\ControllerResolverInterface;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 *
 */
class Qernel extends HttpKernel
{

    /**
     * @param EventDispatcherInterface $dispatcher
     * @param ControllerResolverInterface $resolver
     * @param RequestStack|null $requestStack
     * @param ArgumentResolverInterface|null $argumentResolver
     */
    public function __construct(EventDispatcherInterface $dispatcher, ControllerResolverInterface $resolver, RequestStack $requestStack = null, ArgumentResolverInterface $argumentResolver = null)
    {
        parent::__construct($dispatcher, $resolver, $requestStack, $argumentResolver);


    }


    /**
     * @param Request $request
     * @param int $type
     * @param bool $catch
     * @return Response
     * @throws Exception
     */
    public function handle(Request $request, int $type = HttpKernelInterface::MAIN_REQUEST, bool $catch = true): Response
    {
        $dispatcher = $this->dispatcher;
        return parent::handle($request, $type, $catch);
    }

}