<?php

namespace Qerana\Core\Events;

use Ada\Adapter\XmlAdapter;
use Exception;
use Qerana\Security\QeranaGuard;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;

class SecurityRequestSubscriber implements EventSubscriberInterface
{

    private $dispatcher;

    public function __construct(EventDispatcherInterface $dispatcher){
        $this->dispatcher = $dispatcher;
    }


    public static function getSubscribedEvents(): array
    {
        return ['kernel.request' => 'onRequest'];
    }

    /**
     * @throws Exception
     */
    public function onRequest(RequestEvent $request)
    {


        $req = $request->getRequest();
        $info = $req->getPathInfo();

        // get the module from the url request
        $url_parts = explode('/', $info);
        $request_module = $url_parts[1];


        if ($request_module !== 'login') {

            $xmlModule = new XmlAdapter(__DIR__ . '/../../../../../Framework/Qerana/Config/modules.xml',
                'module');
            // search the module
            $module = $xmlModule->find(['name' => $request_module], ['fetch' => 'one']);

            if ($module !== false) {

                // if not public
                if ($module['access'] !== 'public') {

                    // check access
                    $securityGuard = new QeranaGuard($request->getRequest(),$this->dispatcher);
                    $securityGuard->check();

                }
            }
        }

    }

}