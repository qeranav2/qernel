<?php
declare(strict_types=1);

namespace Qerana\Core;

use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\HttpFoundation\Request;

class Qerana
{


    /**
     * @var mixed
     */
    private $container;

    public function __construct()
    {

        $this->setEnvironment();

        $routes = include __DIR__ . '/routes.php';
        $this->container = include __DIR__ . '/container.php';

        $request = Request::createFromGlobals();
        $response = $this->container->get('Qernel')->handle($request)->send();


    }

    /**
     *
     */
    private function setEnvironment()
    {

        // load env file
        $this->loadEnv();


        switch ($_ENV['ENVIRONMENT']) {
            case 'development':
                ini_set('display_errors', '1');
                error_reporting(-1);
                error_reporting(E_ALL);
                break;

            case 'testing':
            case 'production':
                error_reporting(0);
                break;

            default:
                exit('Qerana dosnt have a valid environment, bye');
        }


    }

    /**
     * Load env file
     */
    private function loadEnv()
    {
        // primero intenta cargar la conf local
        $env_path_local = realpath(__DIR__ . '/../../../../Framework/Qerana/Config/.env.local');

        // si no existe carga la configuracion global
        if ($env_path_local === false) {
            $env_file = realpath(__DIR__ . '/../../../../Framework/Qerana/Config/.env');
        } else {
            $env_file = $env_path_local;
        }

        $dotEnv = new Dotenv();
        $dotEnv->load($env_file);

    }


}