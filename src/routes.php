<?php
declare(strict_types=1);

use Ada\Adapter\XmlAdapter;
use Symfony\Component\Routing;


$routes = new Routing\RouteCollection();
// obtenemos las rutas del archivo xml
$routes_path = __DIR__ . '/../../../../Framework/Qerana/Config/routes.xml';
try {
    $xml = new XmlAdapter($routes_path, 'route');
    $xml_routes = $xml->find();
} catch (Exception $e) {
   throw new \Exception('Routes XML file not found!!');
}

// agregamos todas las rutas
foreach($xml_routes AS $route){
    $routes->add($route['name'], new Routing\Route($route['path'], [
        '_controller' => $route['controller']
    ]));
}


return $routes;