<?php
declare(strict_types=1);

use Ada\Adapter\XmlAdapter;
use App\Welcome\Event\LoginFailSubscriber;
use Qerana\Core\Events\StringResponseSubscriber;
use Qerana\Core\Qernel;
use Symfony\Component\DependencyInjection;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\EventDispatcher;
use Symfony\Component\EventDispatcher\DependencyInjection\RegisterListenersPass;
use Symfony\Component\HttpFoundation;
use Symfony\Component\HttpKernel;
use Symfony\Component\HttpKernel\Controller\ContainerControllerResolver;
use Symfony\Component\Routing;


// Register all routes
$containerBuilder = new DependencyInjection\ContainerBuilder();
$containerBuilder->addCompilerPass(new RegisterListenersPass());
$containerBuilder->register('context', Routing\RequestContext::class);
$containerBuilder->register('matcher', Routing\Matcher\UrlMatcher::class)
    ->setArguments([$routes, new Reference('context')]);

$containerBuilder->register('request_stack', HttpFoundation\RequestStack::class);
$containerBuilder->register('argument_resolver', HttpKernel\Controller\ArgumentResolver::class);

// one listener
$containerBuilder->register('listener.router', HttpKernel\EventListener\RouterListener::class)
    ->setArguments([new Reference('matcher'), new Reference('request_stack')]);

$containerBuilder->register('listener.response', HttpKernel\EventListener\ResponseListener::class)
    ->setArguments(['UTF-8']);


// Exception listener
$containerBuilder->register('listener.exception', HttpKernel\EventListener\ErrorListener::class)
    ->setArguments(['Qerana\Core\Exception\ErrorException::exception']);


$containerBuilder->register('container_controller_resolver', ContainerControllerResolver::class)
    ->setArguments([new Reference('service_container')]);;


$containerBuilder->register('listener.string.response', StringResponseSubscriber::class);
$containerBuilder->register('listener.login.fail', LoginFailSubscriber::class);


$containerBuilder->register('dispatcher', EventDispatcher\EventDispatcher::class)
    ->addMethodCall('addSubscriber', [new Reference('listener.router')])
    ->addMethodCall('addSubscriber', [new Reference('listener.response')])
    ->addMethodCall('addSubscriber', [new Reference('listener.exception')])
    ->addMethodCall('addSubscriber', [new Reference('listener.string.response')])
    ->addMethodCall('addSubscriber', [new Reference('listener.login.fail')]

    );

// add users services
$services_xml = __DIR__ . '/../../../../Framework/Qerana/Config/services.xml';
try {
    $xml = new XmlAdapter($services_xml, 'service');
    $xml_services = $xml->find();
} catch (Exception $e) {
    throw new Exception('Services XML file not found!!');
}


// register all services
foreach ($xml_services as $service) {
    if ($service['active'] === 'true') {

        $class = $service['class'];

        $containerBuilder->register($service['id'], $class);

        // if argument was setted
      if ($service['argument'] != "null") {

        //  dd($service['argument']);

            $definition = $containerBuilder->getDefinition($service['id']);
            $definition->setArguments([new Reference($service['argument'])]);
      }

        if (!empty($service['subscribe']) and $service['subscribe'] !== 'false') {

            $containerBuilder->getDefinition('dispatcher')
                ->addMethodCall('addSubscriber', [new Reference($service['id'])]);

        }
    }

}


$containerBuilder->register('Qernel', Qernel::class)
    ->setArguments([
        new Reference('dispatcher'),
        new Reference('container_controller_resolver'),
        new Reference('request_stack'),
        new Reference('argument_resolver'),
    ]);

return $containerBuilder;