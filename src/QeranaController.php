<?php

namespace Qerana\Core;

use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Loader\FilesystemLoader;

abstract class QeranaController
{

    protected $dispatcher;

    protected $container;

    protected $routes;

    protected $twig_template;

    public function __construct()
    {

        $this->routes = include __DIR__ . '/routes.php';
        $this->container = include __DIR__ . '/container.php';
        $this->dispatcher = $this->container->get('dispatcher');
        $this->twig_template = realpath(__DIR__.'/../../../../src/');

    }


    /**
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws LoaderError
     */
    public function render(string $template,array $params = []){


        $loader = new FilesystemLoader($this->twig_template);
        $twig = new Environment($loader);
        echo $twig->render($template,$params);

    }

    /**
     * @param string $message
     * @return Response
     */
    public function response(string $message): Response
    {
        return new Response($message);
    }


}
