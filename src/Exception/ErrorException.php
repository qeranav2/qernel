<?php

namespace Qerana\Core\Exception;

use Symfony\Component\ErrorHandler\Exception\FlattenException;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Loader\FilesystemLoader;

class ErrorException
{


    /**
     * @var Environment
     */
    private $twig;

    public function __construct(){
        $path = realpath(__DIR__.'/../../../../../Framework/Qerana/Templates/Exceptions');
        $loader = new FilesystemLoader($path);
        $this->twig = new Environment($loader);
    }

    /**
     * @param FlattenException $exception
     * @return Response
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function exception(FlattenException $exception): Response
    {
        $msg = '(' . $exception->getMessage() . ')';
        $t = $this->twig->render('exception.html',['exception' => $msg]);
        return new Response($t, $exception->getStatusCode());
    }

}